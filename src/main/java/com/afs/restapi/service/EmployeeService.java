package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    private final JPAEmployeeRepository jpaEmployeeRepository;


    public EmployeeService(JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return jpaEmployeeRepository.findAll().stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    public EmployeeResponse findById(Long id) {
        Employee employeeById = jpaEmployeeRepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.toResponse(employeeById);
    }

    public void update(Long id, EmployeeRequest employeeRequest) {
        Employee employeeToUpdate = EmployeeMapper.toEntity(employeeRequest);
        Employee employee = EmployeeMapper.responseToEntity(findById(id));
        if (employeeToUpdate.getSalary() != null) {
            employee.setSalary(employeeToUpdate.getSalary());
        }
        if (employeeToUpdate.getAge() != null) {
            employee.setAge(employeeToUpdate.getAge());
        }
        jpaEmployeeRepository.save(employee);
    }

    public List<EmployeeResponse> findAllByGender(String gender) {
        return jpaEmployeeRepository.findByGender(gender).stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        Employee savEmployee = jpaEmployeeRepository.save(employee);
        return EmployeeMapper.toResponse(savEmployee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return jpaEmployeeRepository.findAll(pageable).toList();
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
