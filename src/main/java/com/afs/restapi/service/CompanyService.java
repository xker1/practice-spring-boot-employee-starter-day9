package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    private final JPACompanyRepository jpaCompanyRepository;
    private final JPAEmployeeRepository jpaEmployeeRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return jpaCompanyRepository.findAll(pageable).toList();
    }

    public CompanyResponse findById(Long id) {
        Company company = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        List<Employee> employees = jpaEmployeeRepository.findByCompanyId(company.getId());
        company.setEmployees(employees);
        return CompanyMapper.toResponse(company);
    }

    public void update(Long id, CompanyRequest companyRequest) {
        CompanyResponse companyById = findById(id);
        if (companyRequest.getName() != null) {
            companyById.setName(companyRequest.getName());
        }
        jpaCompanyRepository.save(CompanyMapper.responseToEntity(companyById));
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company entity = CompanyMapper.toEntity(companyRequest);
        Company company = jpaCompanyRepository.save(entity);
        return CompanyMapper.toResponse(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return jpaCompanyRepository.findById(id).map(Company::getEmployees).orElseThrow(CompanyNotFoundException::new);
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
