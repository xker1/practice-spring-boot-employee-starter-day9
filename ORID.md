O:
    1. After learning the concept of Mapper, I have gained a certain understanding of Data Transfer Objects.
    2. Learn to use Flyway as a database management software and understand its role in practical development.
    3. Carry out a retro and presentation, including Microservices, container and CI/CD.
R:  I feel like I learned a lot today and also gained personal ability training.
I: 
    1. At the presentation, our group presented CI/CD, in which I was responsible for answering the issue of visibility and traceability that traditional software lacks that CI/CD addresses. Unfortunately, I am still quite nervous during the presentation and tend to forget words.
    2. In Retro, the team reviewed various aspects of the past period, and in the following, we will strengthen communication among team members and find collaborative spaces for interaction among team members.
D:  I have decided to strengthen my practice and organization of knowledge in the following time, and improve my speaking ability.